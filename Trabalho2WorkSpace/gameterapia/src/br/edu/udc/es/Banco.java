package br.edu.udc.es;

import java.util.LinkedList;

public class Banco {
	LinkedList<Fisioterapeuta> fisioterapeutas = new LinkedList<Fisioterapeuta>();
	LinkedList<Jogador> jogadores = new LinkedList<Jogador>();

	public void deletarFisioterapeuta(String crefito) {
		if(this.verificarSeFisioExiste(crefito)){
		for (int i = 0; i < fisioterapeutas.size(); i++) {
			if (fisioterapeutas.get(i).getCrefito() == crefito) {
				fisioterapeutas.remove(fisioterapeutas.get(i));
				break;
			}
		}
		}else{
			throw new IllegalArgumentException("Fisio Invalido");
		}
	}

	public void adicionarFisioterapeuta(Fisioterapeuta fisio) {
		if (fisio.getCrefito() == null || fisio.getNome() == null) {
			throw new IllegalArgumentException("N�o � possivel");
		}
		fisioterapeutas.add(fisio);
	}

	public boolean verificarSeFisioExiste(String crefito) {
		boolean existe = false;
		for (int i = 0; i < fisioterapeutas.size(); i++) {
			if (fisioterapeutas.get(i).getCrefito() == crefito) {
				fisioterapeutas.remove(fisioterapeutas.get(i));
				existe = true;
				break;
			}
		}
		return existe;
	}

	public int quantidadeFisioterapeuta() {
		return fisioterapeutas.size();
	}

	public void deletarjogador(String nome) {

		for (int i = 0; i < jogadores.size(); i++) {
			if (jogadores.get(i).getNome() == nome) {
				jogadores.remove(jogadores.get(i));
				break;
			}
		}
	}

	public void adicionarJogador(Jogador jogador) {
		if (jogador.getData_nasc() == null || jogador.getNome() == null) {
			throw new IllegalArgumentException("N�o � possivel");
		}
		jogadores.add(jogador);
	}

	public int quantidadeJogadores() {
		return jogadores.size();
	}

}
