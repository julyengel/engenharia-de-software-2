package br.edu.udc.es.testes;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

import br.edu.udc.es.Banco;
import br.edu.udc.es.Fisioterapeuta;
import br.edu.udc.es.Jogador;
import junit.framework.Assert;

public class JogadoresTestes {

	@Test
	public void AdicionarJogadorDevePassar() {
		final Jogador jogador = new Jogador();
		final Banco banco = new Banco();
		jogador.setNome("147852369");
		jogador.setData_nasc(Calendar.getInstance());
		banco.adicionarJogador(jogador);
		Assert.assertEquals(banco.quantidadeJogadores(), 1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void AdicionarJogadorNaoDevePassar() {
		final Jogador jogador = new Jogador();
		final Banco banco = new Banco();
		jogador.setNome("Teste");
		banco.adicionarJogador(jogador);
		Assert.fail("Campo Vazio!");
	}

	@Test(expected = IllegalArgumentException.class)
	public void AdicionarJogadorNaoDevePassarDois() {
		final Jogador jogador = new Jogador();
		final Banco banco = new Banco();
		jogador.setData_nasc(Calendar.getInstance());
		banco.adicionarJogador(jogador);
		Assert.fail("Campo Vazio!");
	}

}
