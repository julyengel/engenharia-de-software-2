package br.edu.udc.es;

import java.util.Calendar;
import java.util.Date;

public class Jogador {
	private String nome;
	private Calendar data_nasc;
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Calendar getData_nasc() {
		return data_nasc;
	}
	public void setData_nasc(Calendar data_nasc) {
		this.data_nasc = data_nasc;
	}
	
}
